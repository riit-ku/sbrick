//
//  div3.cpp
//
//  Created by keno on 2017/07/01.
//  Copyright © 2017 keno. All rights reserved.
//

// Compile
// $ gcc div3.cpp -o div3

// Execution
// $ mpirun -np  4 div3 15  1  1
// $ mpirun -np 12 div3 31 12 20
// $ mpirun -np 16 div3 30 24 24

// 領域サイズと領域分割数を与えて、最適な分割パターンを得る（ノードベース FDM）

#include <SB_SubDomain.h>
#include <stdlib.h>


int main(int argc, char * argv[]) {

  // Check command line arguments
  if ( argc != 4) {
    printf("Usage:\n");
    printf("\t$ mpirun -np N div1 nx ny nz\n");
    return -1;
  }

  int m_sz[3], np=0, myRank=0;
  int proc_grp = 0;
  int gc = 1;      // ガイドセル幅=1

  // conversion from ASCII char to digit
  m_sz[0] = atoi(argv[1]);
  m_sz[1] = atoi(argv[2]);
  m_sz[2] = atoi(argv[3]);

  // Initialize MPI
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

  Hostonly_ printf("%d x %d x %d / %d\n", m_sz[0], m_sz[1], m_sz[2], np);

  SubDomain D(m_sz, gc, np, myRank, proc_grp, MPI_COMM_WORLD, "Cindex");

  bool rflag = true;
  rflag = D.findOptimalDivision();
  MPI_Barrier(MPI_COMM_WORLD);
  if ( !rflag )
  {
    MPI_Finalize();
    Hostonly_ printf("Can't find candidate of division for given parameter\n");
    exit(0);
  }

  rflag = D.createRankTable();
  MPI_Barrier(MPI_COMM_WORLD);
  if ( !rflag )
  {
    MPI_Finalize();
    Hostonly_ printf("Fail to create rank table\n");
    exit(0);
  }

  MPI_Finalize();
  Hostonly_ printf("Successfully terminated.\n\n");

  return 0;
}
