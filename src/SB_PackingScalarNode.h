#ifndef _SB_PACK_S_NODE_H_
#define _SB_PACK_S_NODE_H_

/*
###################################################################################
#
# SBrick
#
# Copyright (c) 2021 Research Institute for Information Technology(RIIT),
#                    Kyushu University.  All rights reserved.
#
####################################################################################
*/

/*
 * @file   SB_PackingScalarNode.h
 * @brief  BrickComm class
 */


// #########################################################
/*
 * @brief pack send data for I direction
 * @param [in]  array   source array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [out] sendm   send buffer of I- direction
 * @param [out] sendp   send buffer of I+ direction
 * @param [in]  nIDm    Rank number of I- direction
 * @param [in]  nIDp    Rank number of I+ direction
 */
template <class T>
void BrickComm::pack_SXnode(const T *array,
                               const int gc,
                               T *sendm,
                               T *sendp,
                               const int nIDm,
                               const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

/*
                 <--gc-->
rankA  [NI-3] [NI-2] [NI-1]  [NI]  [NI+1]
     -----+------+------|------+------+-------> i
rankB   [-2]   [-1]    [0]    [1]    [2]
                               <--gc-->
*/


  // 自領域のデータをマイナス側のランクに送る
  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<gc; i++ ){
          sendm[_IDX_SI(i,j,k,NJ,gc)] = array[_IDX_S3D(i+1,j,k,NI,NJ,VC)];
        }
      }
    }
  }

  // 自領域のデータをプラス側のランクに送る
  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<gc; i++ ){
          sendp[_IDX_SI(i,j,k,NJ,gc)] = array[_IDX_S3D(NI-2+i,j,k,NI,NJ,VC)];
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief unpack send data for I direction
 * @param [out] array   dest array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [in]  recvm   recv buffer of I- direction
 * @param [in]  recvp   recv buffer of I+ direction
 * @param [in]  nIDm    Rank number of I- direction
 * @param [in]  nIDp    Rank number of I+ direction
 */
template <class T>
void BrickComm::unpack_SXnode(T *array,
                                 const int gc,
                                 const T *recvm,
                                 const T *recvp,
                                 const int nIDm,
                                 const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

/*
                 <--gc-->
rankA  [NI-3] [NI-2] [NI-1]  [NI]  [NI+1]
     -----+------+------|------+------+-------> i
rankB   [-2]   [-1]    [0]    [1]    [2]
                               <--gc-->
*/

  // マイナス側からのデータを自領域のガイドセルにコピー
  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<gc; i++ ){
          array[_IDX_S3D(i-1,j,k,NI,NJ,VC)] = recvm[_IDX_SI(i,j,k,NJ,gc)];
        }
      }
    }
  }

  // プラス側からのデータを自領域のガイドセルにコピー
  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<gc; i++ ){
          array[_IDX_S3D(NI+i,j,k,NI,NJ,VC)] = recvp[_IDX_SI(i,j,k,NJ,gc)];
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief pack send data for J direction
 * @param [in]  array   source array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [out] sendm   send buffer of J- direction
 * @param [out] sendp   send buffer of J+ direction
 * @param [in]  nIDm    Rank number of J- direction
 * @param [in]  nIDp    Rank number of J+ direction
 */
template <class T>
void BrickComm::pack_SYnode(const T *array,
                               const int gc,
                               T *sendm,
                               T *sendp,
                               const int nIDm,
                               const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<gc; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          sendm[_IDX_SJ(i,j,k,NI,gc)] = array[_IDX_S3D(i,j+1,k,NI,NJ,VC)];
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<gc; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          sendp[_IDX_SJ(i,j,k,NI,gc)] = array[_IDX_S3D(i,NJ-2+j,k,NI,NJ,VC)];
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief unpack send data for J direction
 * @param [out] array   dest array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [in]  recvm   recv buffer of J- direction
 * @param [in]  recvp   recv buffer of J+ direction
 * @param [in]  nIDm    Rank number of J- direction
 * @param [in]  nIDp    Rank number of J+ direction
 */
template <class T>
void BrickComm::unpack_SYnode(T *array,
                                 const int gc,
                                 const T *recvm,
                                 const T *recvp,
                                 const int nIDm,
                                 const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<gc; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          array[_IDX_S3D(i,j-1,k,NI,NJ,VC)] = recvm[_IDX_SJ(i,j,k,NI,gc)];
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<NK; k++ ){
      for( int j=0; j<gc; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          array[_IDX_S3D(i,NJ+j,k,NI,NJ,VC)] = recvp[_IDX_SJ(i,j,k,NI,gc)];
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief pack send data for K direction
 * @param [in]  array   source array
 * @param [in]  gc      number of guide cell layer actually to be sent
 * @param [out] sendm   send buffer of K- direction
 * @param [out] sendp   send buffer of K+ direction
 * @param [in]  nIDm    Rank number of K- direction
 * @param [in]  nIDp    Rank number of K+ direction
 */
template <class T>
void BrickComm::pack_SZnode(const T *array,
                               const int gc,
                               T *sendm,
                               T *sendp,
                               const int nIDm,
                               const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<gc; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          sendm[_IDX_SK(i,j,k,NI,NJ)] = array[_IDX_S3D(i,j,k+1,NI,NJ,VC)];
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<gc; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          sendp[_IDX_SK(i,j,k,NI,NJ)] = array[_IDX_S3D(i,j,NK-2+k,NI,NJ,VC)];
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief unpack send data for K direction
 * @param [in,out]  array   dest array
 * @param [in]  gc number of guide cell layer to be sent
 * @param [in]  recvm   recv buffer of K- direction
 * @param [in]  recvp   recv buffer of K+ direction
 * @param [in]  nIDm    Rank number of K- direction
 * @param [in]  nIDp    Rank number of K+ direction
 */
template <class T>
void BrickComm::unpack_SZnode(T *array,
                                 const int gc,
                                 const T *recvm,
                                 const T *recvp,
                                 const int nIDm,
                                 const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<gc; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          array[_IDX_S3D(i,j,k-1,NI,NJ,VC)] = recvm[_IDX_SK(i,j,k,NI,NJ)];
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(2)
    for( int k=0; k<gc; k++ ){
      for( int j=0; j<NJ; j++ ){
        #pragma novector
        for( int i=0; i<NI; i++ ){
          array[_IDX_S3D(i,j,NK+k,NI,NJ,VC)] = recvp[_IDX_SK(i,j,k,NI,NJ)];
        }
      }
    }
  }
}


#endif // _SB_PACK_S_NODE_H_
