#ifndef _SB_PACK_V_NODE_H_
#define _SB_PACK_V_NODE_H_

/*
###################################################################################
#
# SBrick
#
# Copyright (c) 2021 Research Institute for Information Technology(RIIT),
#                    Kyushu University.  All rights reserved.
#
####################################################################################
*/

/*
 * @file   SB_PackingVectorNode.h
 * @brief  BrickComm class
 */


// #########################################################
/*
 * @brief pack send data for I direction
 * @param [in]  array   source array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [out] sendm   send buffer of I- direction
 * @param [out] sendp   send buffer of I+ direction
 * @param [in]  nIDm    Rank number of I- direction
 * @param [in]  nIDp    Rank number of I+ direction
 */
template <class T>
void BrickComm::pack_VXnode(const T *array,
                               const int gc,
                               T *sendm,
                               T *sendp,
                               const int nIDm,
                               const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  /*
                   <--gc-->
  rankA  [NI-3] [NI-2] [NI-1]  [NI]  [NI+1]
       -----+------+------|------+------+-------> i
  rankB   [-2]   [-1]    [0]    [1]    [2]
                                 <--gc-->
  */

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<gc; i++ ){
            sendm[_IDX_VI(i,j,k,l,NJ,NK,gc)] = array[_IDX_V3D(i+1,j,k,l,NI,NJ,NK,VC)];
          }
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<gc; i++ ){
            sendp[_IDX_VI(i,j,k,l,NJ,NK,gc)] = array[_IDX_V3D(NI-2+i,j,k,l,NI,NJ,NK,VC)];
          }
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief unpack send data for I direction
 * @param [out] array   dest array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [in]  recvm   recv buffer of I- direction
 * @param [in]  recvp   recv buffer of I+ direction
 * @param [in]  nIDm    Rank number of I- direction
 * @param [in]  nIDp    Rank number of I+ direction
 */
template <class T>
void BrickComm::unpack_VXnode(T *array,
                                 const int gc,
                                 const T *recvm,
                                 const T *recvp,
                                 const int nIDm,
                                 const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<gc; i++ ){
            array[_IDX_V3D(i-1,j,k,l,NI,NJ,NK,VC)] = recvm[_IDX_VI(i,j,k,l,NJ,NK,gc)];
          }
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<gc; i++ ){
            array[_IDX_V3D(NI+i,j,k,l,NI,NJ,NK,VC)] = recvp[_IDX_VI(i,j,k,l,NJ,NK,gc)];
          }
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief pack send data for J direction
 * @param [in]  array   source array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [out] sendm   send buffer of J- direction
 * @param [out] sendp   send buffer of J+ direction
 * @param [in]  nIDm    Rank number of J- direction
 * @param [in]  nIDp    Rank number of J+ direction
 */
template <class T>
void BrickComm::pack_VYnode(const T *array,
                               const int gc,
                               T *sendm,
                               T *sendp,
                               const int nIDm,
                               const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<gc; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            sendm[_IDX_VJ(i,j,k,l,NI,NK,gc)] = array[_IDX_V3D(i,j+1,k,l,NI,NJ,NK,VC)];
          }
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<gc; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            sendp[_IDX_VJ(i,j,k,l,NI,NK,gc)] = array[_IDX_V3D(i,NJ-2+j,k,l,NI,NJ,NK,VC)];
          }
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief unpack send data for J direction
 * @param [out] array   dest array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [in]  recvm   recv buffer of J- direction
 * @param [in]  recvp   recv buffer of J+ direction
 * @param [in]  nIDm    Rank number of J- direction
 * @param [in]  nIDp    Rank number of J+ direction
 */
template <class T>
void BrickComm::unpack_VYnode(T *array,
                                 const int gc,
                                 const T *recvm,
                                 const T *recvp,
                                 const int nIDm,
                                 const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<gc; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            array[_IDX_V3D(i,j-1,k,l,NI,NJ,NK,VC)] = recvm[_IDX_VJ(i,j,k,l,NI,NK,gc)];
          }
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<NK; k++ ){
        for( int j=0; j<gc; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            array[_IDX_V3D(i,NJ+j,k,l,NI,NJ,NK,VC)] = recvp[_IDX_VJ(i,j,k,l,NI,NK,gc)];
          }
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief pack send data for K direction
 * @param [in]  array   source array
 * @param [in]  gc      number of guide cell layer actually to be sent
 * @param [out] sendm   send buffer of K- direction
 * @param [out] sendp   send buffer of K+ direction
 * @param [in]  nIDm    Rank number of K- direction
 * @param [in]  nIDp    Rank number of K+ direction
 */
template <class T>
void BrickComm::pack_VZnode(const T *array,
                               const int gc,
                               T *sendm,
                               T *sendp,
                               const int nIDm,
                               const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<gc; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            sendm[_IDX_VK(i,j,k,l,NI,NJ,gc)] = array[_IDX_V3D(i,j,k+1,l,NI,NJ,NK,VC)];
          }
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<gc; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            sendp[_IDX_VK(i,j,k,l,NI,NJ,gc)] = array[_IDX_V3D(i,j,NK-2+k,l,NI,NJ,NK,VC)];
          }
        }
      }
    }
  }
}


// #########################################################
/*
 * @brief unpack send data for K direction
 * @param [out] array   dest array
 * @param [in]  gc      number of guide cell layer to be sent
 * @param [in]  recvm   recv buffer of K- direction
 * @param [in]  recvp   recv buffer of K+ direction
 * @param [in]  nIDm    Rank number of K- direction
 * @param [in]  nIDp    Rank number of K+ direction
 */
template <class T>
void BrickComm::unpack_VZnode(T *array,
                                 const int gc,
                                 const T *recvm,
                                 const T *recvp,
                                 const int nIDm,
                                 const int nIDp)
{
  int NI = size[0];
  int NJ = size[1];
  int NK = size[2];
  int VC = halo_width;

  if( nIDm >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<gc; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            array[_IDX_V3D(i,j,k-1,l,NI,NJ,NK,VC)] = recvm[_IDX_VK(i,j,k,l,NI,NJ,gc)];
          }
        }
      }
    }
  }

  if( nIDp >= 0 )
  {
#pragma omp parallel for collapse(3)
    for (int l=0; l<3; l++) {
      for( int k=0; k<gc; k++ ){
        for( int j=0; j<NJ; j++ ){
          #pragma novector
          for( int i=0; i<NI; i++ ){
            array[_IDX_V3D(i,j,NK+k,l,NI,NJ,NK,VC)] = recvp[_IDX_VK(i,j,k,l,NI,NJ,gc)];
          }
        }
      }
    }
  }
}


#endif // _SB_PACK_V_NODE_H_
